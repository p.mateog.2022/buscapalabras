#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

import sortwords
import sys

def search_word(word, words_list):
    found= False
    for i in words_list:
        if sortwords.equal(word, i):
            index1=words_list.index(i)
            found=True
            return index1
    if not found:
        raise Exception



def main():
    word = sys.argv[1]
    world_list=sys.argv[2:]

    if len(sys.argv) < 3:
        sys.exit("At least two arguments needed")

    try:
        ordered_list =sortwords.sort(words_list)
        index = search_word(word,ordered_list)
        sortwords.show(ordered_list)
        print(index)
    except Exception:
        sys.exit('World not found')


if __name__ == '__main__':
    main()
